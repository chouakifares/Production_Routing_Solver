# Production Routing Solver

In this project we are interested in creating a solver for a well known optimisation problem the **Production Routing Problem**.Our goal is to implements several types of solutions for this problem.We start by proposing a greedy heuristic solution then we move to creating a compact linear program that finds the optimal solution for this problem, at last we try to achieve better performances using a non compact linear program.

A full description of this problem and the work that is done in this project can be found in the file `Projet_Production_Routing22-23.pdf`
