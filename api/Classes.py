from cplex import *
from docplex.mp.model import Model
from cplex.callbacks import LazyConstraintCallback, UserCutCallback, HeuristicCallback
from docplex.mp.callbacks.cb_mixin import *
from itertools import combinations
import numpy as np

class Instance:
    BIG_M = 1000
    MC = 15
    def __init__(self,type, nb_periods, producer, buyers):
        """
            type(str)(type A or type B instance)
            nb_periods(int)(l)
            producer(Producer)
            buyers([Buyer])
        """
        self.type = type
        self.nb_periods = nb_periods
        self.producer = producer
        self.buyers = buyers
        self.distance_matrix = self.create_distance_matrix()
        self.M_params = [
            min(
                producer.production_capacity,
                sum(
                    [
                        buyers[i].demand[j]
                            for i in range(len(buyers))
                                for j in range(t, nb_periods)
                    ]
                )
            )
                for t in range(nb_periods)
        ]

        self.M_tilde_params = [
            [
                min(
                    producer.vehicule_storage_capacity,
                    buyers[i].storage_limit,
                    sum(
                        [
                            buyers[i].demand[j]
                                for j in range(t, nb_periods)
                        ]
                    )
                )
                    for t in range(nb_periods)
            ]
                for i in range(len(buyers))
        ]

    def create_distance_matrix(self):
        def distance(type, x1,y1, x2,y2):
            if(self.type == "A"):
                return np.floor(np.sqrt((x1 - x2)**2 +(y1 - y2)**2) + 1/2)
            else:
                return Instance.MC * np.sqrt((x1 - x2)**2 +(y1 - y2)**2)

        distance_matrix = np.zeros((len(self.buyers)+1,len(self.buyers)+1))
        #distance from producer to each buyer
        for i in range(len(self.buyers)):
                dist = distance(
                    self.type,
                    self.producer.x_coord,
                    self.producer.y_coord,
                    self.buyers[i].x_coord,
                    self.buyers[i].y_coord
                )
                distance_matrix[0,i+1] = dist
                distance_matrix[i+1,0] = dist
        for i in range(1,len(self.buyers)+1):
            for j in range(i+1, len(self.buyers)+1):
                dist = distance(
                    self.type,
                    self.buyers[i-1].x_coord,
                    self.buyers[i-1].y_coord,
                    self.buyers[j-1].x_coord,
                    self.buyers[j-1].y_coord
                )
                distance_matrix[i,j] = dist
                distance_matrix[j,i] = dist

        return distance_matrix

    """
        function that creates and solves the lot sizing problem
    """
    def solve_lsp(self, SC = None):
        m = Model()

        #Creating the variables
        p_vars, y_vars, I_vars, q_vars, z_vars = (
            [],
            [],
            [
                [
                    m.continuous_var(name=f"I_0_{t}", lb=0)
                        for t in range(self.nb_periods)
                ]
            ],
            [],
            [])
        for t in range(self.nb_periods):
            p_vars.append(m.continuous_var(name=f"p_{t}", lb=0))
            y_vars.append(m.binary_var(name=f"y_{t}"))

        for i in range(len(self.buyers)):
            tmp_I, tmp_q, tmp_z = [],[],[]
            for t in range(self.nb_periods):
                tmp_I.append(m.continuous_var(name=f"I_{i+1}_{t}", lb=0))
                tmp_q.append(m.continuous_var(name=f"q_{i}_{t}", lb=0))
                tmp_z.append(m.binary_var(name=f"z_{i}_{t}"))
            I_vars.append(tmp_I)
            q_vars.append(tmp_q)
            z_vars.append(tmp_z)



        #Adding the objective function
        #list of objectives , one for each time step
        objectives = []
        for t in range(self.nb_periods):
            production_cost_expr = self.producer.production_cost * p_vars[t]
            setup_cost_expr = self.producer.setup_cost * y_vars[t]
            storage_cost_expr = self.producer.storage_cost * I_vars[0][t]
            transportation_cost = 0
            for i in range(len(self.buyers)):
                storage_cost_expr += self.buyers[i].storage_cost * I_vars[i+1][t]
                transportation_cost += SC[i][t] * z_vars[i][t]
            objectives.append(
                production_cost_expr
                + setup_cost_expr
                + storage_cost_expr
                + transportation_cost
            )
        m.minimize(sum(objectives))



        #creating the constraints
        #constraint 1
        for t in range(self.nb_periods):
            tmp = 0
            for i in range(len(self.buyers)):
                tmp += q_vars[i][t]
            if(t>0):
                m.add_constraint(I_vars[0][t-1] + p_vars[t] == tmp + I_vars[0][t])
            else:
                m.add_constraint(self.producer.initial_stock + p_vars[t] == tmp + I_vars[0][t])

        #constraint 2
        for t in range(self.nb_periods):
            for i in range(len(self.buyers)):
                if(t > 0):
                    m.add_constraint(I_vars[i+1][t-1] + q_vars[i][t] == self.buyers[i].demand[t] + I_vars[i+1][t])
                else:
                    m.add_constraint(self.buyers[i].initial_stock + q_vars[i][t] == self.buyers[i].demand[t] + I_vars[i+1][t])

        #constraint 3
        for t in range(self.nb_periods):
            m.add_constraint(p_vars[t] <= self.M_params[t] * y_vars[t])

        #constraint 4
        for t in range(self.nb_periods):
            if(t>0):
                m.add_constraint(I_vars[0][t-1] <= self.producer.storage_limit)
            else:
                m.add_constraint(self.producer.initial_stock <= self.producer.storage_limit)
        #constraint 5
        for t in range(self.nb_periods):
            for i in range(len(self.buyers)):
                if(t>0):
                    m.add_constraint(
                        I_vars[i+1][t-1] + q_vars[i][t]
                        <= self.buyers[i].storage_limit
                    )
                else:
                    m.add_constraint(
                        self.buyers[i].initial_stock + q_vars[i][t]
                        <= self.buyers[i].storage_limit
                    )
        for t in range(self.nb_periods):
            for i in range(len(self.buyers)):
                m.add_constraint(q_vars[i][t] <= self.M_tilde_params[i][t] * z_vars[i][t])
        s = m.solve()

        return {
            "q_vars":[[s.get_value(j.name) for j in i] for i in q_vars],
            "p_vars":[s.get_value(i.name) for i in p_vars],
            "y_vars":[s.get_value(i.name) for i in y_vars],
            "I_vars":[[s.get_value(j.name) for j in i] for i in I_vars],
            "z_vars":[[s.get_value(j.name) for j in i] for i in z_vars],
            "objective_value":s.get_objective_value()
        }

    def solve_bin_packing_exact(self,demands):
        m = Model()

        #assignement vars (x_i_j: 1 if we assign client i to truck j)
        x_vars = [
            [
                m.binary_var(name=f"x_{i}_{j}")
                    for i in range(len(self.buyers))
            ]
            for j in range(self.producer.nb_vehicules)
        ]

        # wether a truck is used or not
        y_vars = [
            m.binary_var(name=f"y_{i}")
                for i in range(self.producer.nb_vehicules)
        ]

        #constraints on the size of the merch that each truck takes
        for j in range(self.producer.nb_vehicules):
            m.add_constraint(
                sum(
                    [
                        x_vars[j][i] * demands[i]
                            for i in range(len(self.buyers))
                    ]
                ) <= y_vars[j] * self.producer.vehicule_storage_capacity
            )
        #constraints on that each client is delivered by at least one truck
        for i in range(len(self.buyers)):
            if(demands[i]):
                m.add_constraint(
                     sum(
                        [
                            x_vars[j][i] * y_vars[j]
                                for j in range(self.producer.nb_vehicules)
                        ]
                    ) >=1
                )

        #minimize the number of trucks used
        m.minimize(sum(y_vars))
        solution =  m.solve()
        return (
            [[solution.get_value(var.name) for var in i] for i in x_vars],
            [solution.get_value(var.name)  for var in y_vars],
            solution.get_objective_value()
        )

    def clark_wright_heuristic(self,demands):
        max_storage = self.producer.vehicule_storage_capacity
        total_savings = 0
        circuits = [[i] for i in range(len(self.buyers)) if demands[i]]
        savings_matrix = np.zeros((len(self.buyers), len(self.buyers)))
        ordered_savings = []
        for i in range(len(self.buyers)):
            for j in range(len(self.buyers)):
                savings_matrix[i,j] = (
                    self.distance_matrix[0, i+1]
                    + self.distance_matrix[0, j+1]
                    - self.distance_matrix[i+1, j+1]
                )
                savings_matrix[j,i] = savings_matrix[i,j]
        tmp = []
        for i in range(len(self.buyers)):
            for j in range(len(self.buyers)):
                tmp.append((savings_matrix[i,j], i , j))
        ordered_savings = list(
            filter(
                lambda tup: tup[0]>0,
                sorted(
                    tmp,
                    key=lambda tup:tup[0],
                    reverse = True
                )
            )
        )
        for item in ordered_savings:
            #check if we can deliver client i in the same truck as client j
            if(demands[item[1]] and demands[item[2]]):
                for c in circuits:
                    #if client 1 is delivered in this circuit
                    if item[1] in c and item[2] not in c:
                        #if the truck that delivers client 1 can fit the delivery of
                        #client 2
                        if(
                            sum(
                                [demands[i] for i in c]
                                + [demands[item[2]]]
                            ) < max_storage
                        ):
                            #delete the item that will be merged from its current circuit
                            for tmp in circuits:
                                if(item[2] in tmp):
                                    tmp.remove(item[2])
                                    if(not len(tmp)):
                                        circuits.remove([])
                                    break
                            #add the item to the new circuit
                            c.append(item[2])
                            total_savings+= item[0]
                            break
                    elif item[2] in c and item[1] not in c:
                        #if the truck that delivers client 2 can fit the delivery of
                        #client 1
                        if(
                            sum(
                                [demands[i] for i in c]
                                + [demands[item[1]]]
                            ) < max_storage
                        ):
                            #merge the two cycle together
                            c.append(item[1])
                            total_savings+= item[0]

                            #delete the merged item from its previous circuit
                            for tmp in circuits:
                                if(item[1] in tmp):
                                    tmp.remove(item[1])
                                    if(not len(tmp)):
                                        circuits.remove([])
                                    break

                            break
        return circuits, total_savings

    def genetic_algo_for_tsp(self,nodes,nb_gen=100, pop_size=1000,mutation_proba=0.2, verbose = False):

        def get_neighbors(ind):
            neighbors = []
            for i in range(1,len(ind)//2):
                new_ind = [j for j in ind]
                new_ind[i], new_ind[-i] = new_ind[-i],new_ind[i]
                neighbors.append(new_ind)
            return neighbors

        def fitness(ind):
            perf = self.distance_matrix[0,ind[0]+1]
            for i in range(len(ind)-1):
                perf+=self.distance_matrix[ind[i]+1,ind[i+1]+1]
            perf+=self.distance_matrix[ind[len(ind)-1]+1,0]
            return perf

        def crossover(ind1, ind2):
            def encode(ind):
                ind = list(ind)
                return [ind.index(i) for i in sorted(ind)]

            def decode(values,code):
                return [values[i] for i in code]

            values = sorted(ind1)
            ind1, ind2 = encode(ind1), encode(ind2)
            size = min(len(ind1), len(ind2))
            p1, p2 = [0] * size, [0] * size

            # Initialize the position of each indices in the individuals
            for i in range(size):
                p1[ind1[i]] = i
                p2[ind2[i]] = i
            # Choose crossover points
            cxpoint1 = np.random.randint(0, size)
            cxpoint2 = np.random.randint(0, size - 1)
            if cxpoint2 >= cxpoint1:
                cxpoint2 += 1
            else:  # Swap the two cx points
                cxpoint1, cxpoint2 = cxpoint2, cxpoint1

            # Apply crossover between cx points
            for i in range(cxpoint1, cxpoint2):
                # Keep track of the selected values
                temp1 = ind1[i]
                temp2 = ind2[i]
                # Swap the matched value
                ind1[i], ind1[p1[temp2]] = temp2, temp1
                ind2[i], ind2[p2[temp1]] = temp1, temp2
                # Position bookkeeping
                p1[temp1], p1[temp2] = p1[temp2], p1[temp1]
                p2[temp1], p2[temp2] = p2[temp2], p2[temp1]

            return decode(values,ind1), decode(values,ind2)

        def selection(perfs, k):
            best = np.argpartition(perfs, -k)[-k:]
            best_perfs = np.array(perfs)[best]
            proba = best_perfs/np.sum(best_perfs)
            return np.random.choice(best, 2, p = proba)

        def mutation(ind):
            pt1, pt2 = np.random.choice(range(len(ind)), 2)
            new_ind = [i for i in ind]
            new_ind[pt1], new_ind[pt2] = ind[pt2], ind[pt1]
            return new_ind

        best_ind, best_fitness = None, None
        population = [np.random.permutation(nodes) for i in range(pop_size)]
        for _ in range(nb_gen):
            #evaluate the individuals
            perfs= []
            for individual in population:
                perf = fitness(individual)
                if best_fitness is None or perf < best_fitness:
                    best_fitness = perf
                    best_ind = individual
                    if(verbose):
                        print(f"new best individual found with performance {best_fitness}")
                perfs.append(perf)
            children = []
            while len(children) != len(population):
                tmp = selection(perfs, 100)
                index_parent1 = tmp[0]
                index_parent2 = tmp[1]
                c1, c2 = crossover(population[index_parent1], population[index_parent2])
                if(np.random.rand()<=mutation_proba): c1 = mutation(c1)
                if(np.random.rand()<=mutation_proba): c2 = mutation(c2)
                children.append(c1)
                children.append(c2)
            population = children
        return best_ind

    def solve_prp_exact(self, GFSEC = False, gap=0.1, timelimit = 300):

        def find_constraint(sol, const_set):
            nb_periods = self.nb_periods
            nb_buyers = len(self.buyers)
            best_constraint_at_t = []
            for t in range(nb_periods):
                done = False
                to_consider = set()
                for i in range(nb_buyers):
                    for j in range(nb_buyers):
                        if i!=j:
                            if(sol.get_value(f"x_{i+1}_{j+1}_{t}") > 0):
                                to_consider.add(i)
                                to_consider.add(j)
                if(len(to_consider) > 1):
                    factors =[]
                    for card_S in range(1,nb_buyers+1):
                        curr_combos = combinations(to_consider, card_S)
                        found = 0
                        for S in curr_combos:
                            tmp_q = sum(
                                [
                                    sol.get_value(f"q_{i}_{t}")
                                    for i in S
                                ]
                            )
                            B_minus_S = [i for i in range(nb_buyers+1) if i not in [j+1 for j in S]]
                            tmp_x = 0
                            for i in B_minus_S:
                                for j in S:
                                    if i!=j:
                                        tmp_x += sol.get_value(f"x_{i}_{j+1}_{t}")

                            Q = self.producer.vehicule_storage_capacity
                            if(tmp_x < tmp_q/Q):
                                left_member_vars = []
                                left_member_coeffs = []
                                for i in B_minus_S:
                                    for j in S:
                                        if i != j+1:
                                            left_member_vars.append(f"x_{i}_{j+1}_{t}")
                                            left_member_coeffs.append(1)
                                for i in S:
                                    left_member_vars.append(f"q_{i}_{t}")
                                    left_member_coeffs.append(-1/Q)

                                left_member_vars = tuple(left_member_vars)
                                left_member_coeffs = tuple(left_member_coeffs)
                                if((left_member_vars,left_member_coeffs) not in const_set):
                                    if tmp_q/Q:
                                        factors.append((
                                             tmp_x /(tmp_q/Q) ,
                                            (left_member_vars, left_member_coeffs))
                                        )
                                    else:
                                        factors.append((
                                             Instance.BIG_M ,
                                            (left_member_vars, left_member_coeffs))
                                        )
                                found += 1
                        if(found>10):
                            break

                    factors = sorted(factors, key= lambda e:e[0])
                    for i in factors:
                        best_constraint_at_t.append(i)
            best_constraint_at_t = sorted(best_constraint_at_t, key= lambda e:e[0])
            if(len(best_constraint_at_t)):
                return[i[1] for i in best_constraint_at_t]
            else:
                return (False,)

        class GFSECLazyCallback(ConstraintCallbackMixin, LazyConstraintCallback):
            def __init__(self, env):
                LazyConstraintCallback.__init__(self, env)
                ConstraintCallbackMixin.__init__(self)
                self.const_set = set()
                self.nb_lazy_cts = 0

            def add_lazy_constraints(self, cts):
                self.register_constraints(cts)

            #@print_called(f'--> lazy constraint callback called: #')
            def __call__(self):
                # fetch variable values into a solution
                constraints_found = find_constraint(self.make_solution(), self.const_set)
                if(len(constraints_found)>1 or constraints_found[0] != False):
                    for constraint in constraints_found:
                        self.nb_lazy_cts +=1
                        self.const_set.add((constraint[0],constraint[1]))
                        left_member = SparsePair(
                            ind = constraint[0],
                            val = constraint[1]
                        )
                        self.add(
                            left_member,
                            "G",
                            0
                        )

                    #print(f'new lazy constraint[{self.nb_lazy_cts}]')

        class GFSECUserCutCallback(ConstraintCallbackMixin, UserCutCallback):
            def __init__(self, env):
                UserCutCallback.__init__(self, env)
                ConstraintCallbackMixin.__init__(self)
                self.const_set = set()
                self.nb_user_cut_cts = 0
            def add_lazy_constraints(self, cts):
                self.register_constraints(cts)

            #@print_called('--> user cut callback called: #{0}')
            def __call__(self):
                # fetch variable values into a solution
                constraints_found = find_constraint(self.make_solution(), self.const_set)
                if(len(constraints_found)>1 or constraints_found[0] != False):
                    for constraint in constraints_found:
                        self.nb_user_cut_cts +=1
                        self.const_set.add((constraint[0],constraint[1]))
                        left_member = SparsePair(
                            ind = constraint[0],
                            val = constraint[1]
                        )
                        self.add(
                            left_member,
                            "G",
                            0
                        )

                    #print(f'new user cut constraint[{self.nb_user_cut_cts}]')

        class RoundingCallback(ModelCallbackMixin, HeuristicCallback):
            def __init__(self, env):
                HeuristicCallback.__init__(self, env)
                ModelCallbackMixin.__init__(self)

            def __call__(self):
                feas = self.get_feasibilities()
                infeasible_indices = [j for j, f in enumerate(feas) if f == self.feasibility_status.infeasible]
                feasible_indices = [j for j, f in enumerate(feas) if f == self.feasibility_status.feasible]
                if infeasible_indices:
                    # this shows how to get back to the DOcplex variable from the index
                    # but is not necessary for the logic.
                    infeasible_vars = [self.index_to_var(v) for v in infeasible_indices]
                    feasible_vars = [self.index_to_var(v) for v in feasible_indices]

                    new_sol = [
                        [v for v in feasible_indices],
                        [self.get_values(v) for v in feasible_indices]
                    ]
                    #this rounding scheme doesn't work
                    #new_sol[0] += [v for v in infeasible_indices]
                    #new_sol[1] += [1 for v in infeasible_indices]

                    #circuits = self.clark_wright_heuristic(q_vars[:,t])


                    #self.set_solution(new_sol)

        model = Model()
        model.context.solver.log_output = True
        #decision vars
        p_vars = {}
        y_vars = {}
        z_vars = {}
        x_vars = {}
        I_vars = {}
        q_vars = {}
        w_vars = {}

        for t in range(self.nb_periods):
            p_vars[f"p_{t}"] = model.continuous_var(name=f"p_{t}", lb=0)
            y_vars[f"y_{t}"] = model.binary_var(name=f"y_{t}")
            z_vars[f"z_0_{t}"] = model.integer_var(
                name=f"z_0_{t}",
                lb=0,
                ub = self.producer.nb_vehicules
            )
            for i in range(len(self.buyers)):
                z_vars[f"z_{i+1}_{t}"] = model.binary_var(name=f"z_{i+1}_{t}")
                q_vars[f"q_{i}_{t}"] = model.continuous_var(name=f"q_{i}_{t}", lb=0)

            for i in range(len(self.buyers)+1):
                I_vars[f"I_{i}_{t}"] = model.continuous_var(
                    name=f"I_{i}_{t}",
                    lb=0
                )

                for j in range(len(self.buyers) + 1):
                    if(i != j):
                        x_vars[f"x_{i}_{j}_{t}"] = model.binary_var(name=f"x_{i}_{j}_{t}")



        #define objective
        objectives = []
        for t in range(self.nb_periods):
            production_cost = (
                self.producer.production_cost * p_vars[f"p_{t}"]
                + self.producer.setup_cost * y_vars[f"y_{t}"]
            )
            storage_cost = self.producer.storage_cost * I_vars[f"I_0_{t}"]
            transportation_cost = 0
            for j in range(len(self.buyers)):
                transportation_cost += (
                    x_vars[f"x_0_{j+1}_{t}"] * self.distance_matrix[0][j+1]
                ) + (
                    x_vars[f"x_{j+1}_0_{t}"] * self.distance_matrix[j+1][0]
                )
            for i in range(len(self.buyers)):
                storage_cost += self.buyers[i].storage_cost * I_vars[f"I_{i+1}_{t}"]
                for j in range(len(self.buyers)):
                    if(j!=i):
                        transportation_cost += (
                            x_vars[f"x_{i+1}_{j+1}_{t}"]
                            * self.distance_matrix[i+1][j+1]
                        )
            objectives.append(production_cost
                + storage_cost
                + transportation_cost)

        model.parameters.mip.tolerances.mipgap.set(gap)
        if(timelimit > 0):
            model.parameters.timelimit=timelimit;
        model.minimize(sum(objectives))

        for t in range(self.nb_periods):
            #cnostraint 2
            tmp_q = 0
            for i in range(len(self.buyers)):
                tmp_q += q_vars[f"q_{i}_{t}"]
            if t > 0:
                model.add_constraint(
                    I_vars[f"I_{0}_{t-1}"] + p_vars[f"p_{t}"]
                    == tmp_q + I_vars[f"I_{0}_{t}"]
                )
            else:
                model.add_constraint(
                    self.producer.initial_stock + p_vars[f"p_{t}"]
                    == tmp_q + I_vars[f"I_{0}_{t}"]
                )
            #constraint 3
            for i in range(len(self.buyers)):
                if t > 0:
                    model.add_constraint(
                        I_vars[f"I_{i+1}_{t-1}"] + q_vars[f"q_{i}_{t}"]
                        == self.buyers[i].demand[t] + I_vars[f"I_{i+1}_{t}"]
                    )
                else:
                    model.add_constraint(
                        self.buyers[i].initial_stock + q_vars[f"q_{i}_{t}"]
                        == self.buyers[i].demand[t] + I_vars[f"I_{i+1}_{t}"]
                    )
            #constraint 4
            model.add_constraint(
                p_vars[f"p_{t}"] <= self.M_params[t] * y_vars[f"y_{t}"]
            )
            #constraint 5
            model.add_constraint(I_vars[f"I_0_{t}"] <= self.producer.storage_limit)
            #constraint 6
            for i in range(len(self.buyers)):
                if t > 0:
                    model.add_constraint(
                        I_vars[f"I_{i+1}_{t-1}"] + q_vars[f"q_{i}_{t}"]
                        <=  self.buyers[i].storage_limit
                    )
                else:
                    model.add_constraint(
                        self.buyers[i].initial_stock + q_vars[f"q_{i}_{t}"]
                        <= self.buyers[i].storage_limit
                    )
            #constraint 7
            for i in range(len(self.buyers)):
                model.add_constraint(
                    q_vars[f"q_{i}_{t}"]
                    <= self.M_tilde_params[i][t] * z_vars[f"z_{i+1}_{t}"]
                )
            #constraint 8
            for i in range(len(self.buyers)):
                tmp_xi = 0
                for j in range(len(self.buyers)+1):
                    if i+1!= j:
                        tmp_xi += x_vars[f"x_{i+1}_{j}_{t}"]
                model.add_constraint(tmp_xi == z_vars[f"z_{i+1}_{t}"])

            #constaint 9
            for i in range(len(self.buyers)+1):
                tmp_xi = 0
                for j in range(len(self.buyers)+1):
                    if i != j:
                        tmp_xi += (
                            x_vars[f"x_{i}_{j}_{t}"]
                            + x_vars[f"x_{j}_{i}_{t}"]
                        )
                model.add_constraint(tmp_xi == 2 * z_vars[f"z_{i}_{t}"])
            #constraint 10
            model.add_constraint(
                z_vars[f"z_0_{t}"]
                <= self.producer.nb_vehicules
            )



        if(GFSEC):
            model.register_callback(GFSECLazyCallback)
            model.register_callback(GFSECUserCutCallback)
        else:
            #constraint 12
            for t in range(self.nb_periods):
                for i in range(len(self.buyers)):
                    w_vars[f"w_{i}_{t}"] = model.continuous_var(name=f"w_{i}_{t}", lb=0)

            for t in range(self.nb_periods):
                for i in range(len(self.buyers)):
                    model.add_constraint(
                        w_vars[f"w_{i}_{t}"]
                        <= (
                            self.producer.vehicule_storage_capacity
                            * z_vars[f"z_{i+1}_{t}"]
                            )
                    )
                    model.add_constraint(w_vars[f"w_{i}_{t}"] >= 0)

                    #constraint 11
                    for j in range(len(self.buyers)):
                        if i!=j:
                            model.add_constraint(
                                w_vars[f"w_{i}_{t}"] - w_vars[f"w_{j}_{t}"]
                                >= (
                                    q_vars[f"q_{i}_{t}"]
                                    - self.M_tilde_params[i][t] * (1 - x_vars[f"x_{i+1}_{j+1}_{t}"])
                                    )
                            )

        #model.register_callback(RoundingCallback)

        model.print_information()
        s = model.solve()
        print(s)
        solve_status = model.get_solve_status()
        if solve_status.name == 'INFEASIBLE_SOLUTION':
            import docplex.mp.conflict_refiner as cr
            cref = cr.ConflictRefiner()
            print('show some of the constraints that can be removed to arrive at a minimal conflict')
            cref.display_conflicts(cref.refine_conflict(model, display=True))


        to_return={
            "objective":s.get_objective_value(),
            "gap":model.solve_details.mip_relative_gap,
            "best_bound":model.solve_details.best_bound
        }
        for i in range(self.nb_periods):
            to_return[i] = {
                "p":round(s.get_value(f"p_{i}")),
                "y":round(s.get_value(f"y_{i}")) ,
                "z":{j:round(s.get_value(j)) for j in z_vars.keys() if int(j[-1])==i} ,
                "x_vars":{j:round(s.get_value(j)) for j in x_vars.keys() if int(j[-1])==i} ,
                "I_vars":{j:round(s.get_value(j)) for j in I_vars.keys() if int(j[-1])==i} ,
                "q_vars":{j:round(s.get_value(j)) for j in q_vars.keys() if int(j[-1])==i} ,
                "w_vars":{j:round(s.get_value(j)) for j in w_vars.keys() if int(j[-1])==i} ,
            }
        self.specify_costs(to_return)
        return to_return

    def heuristic_solve(self, max_iter = 1):
        #initialize SC
        SC = np.zeros((len(self.buyers), self.nb_periods))
        for i in range(len(self.buyers)):
            init_value = self.distance_matrix[0,i+1] + self.distance_matrix[i+1,0]
            for t in range(self.nb_periods):
                SC[i, t] = init_value
        new_SC = np.zeros((len(self.buyers), self.nb_periods))
        solution  = self.solve_lsp(SC)
        obj_value = solution["objective_value"]
        q_vars = solution["q_vars"]

        q_vars = np.array(q_vars)
        all_circuits = []
        for t in range(q_vars.shape[1]):
            circuits, savings = self.clark_wright_heuristic(q_vars[:,t])
            for j in range(len(circuits)):
                if(len(circuits[j])> 1):
                    improved_circuit = self.genetic_algo_for_tsp(
                        circuits[j],
                        2**min(len(circuits[j]),10),
                        100,
                        verbose = False
                    )
                    new_SC[improved_circuit[0],t] =(
                        self.distance_matrix[
                            improved_circuit[0]+1,
                            improved_circuit[1]+1]
                        + self.distance_matrix[0,improved_circuit[0]+1]
                        - self.distance_matrix[0,improved_circuit[1]+1]
                    )
                    for i in range(1,len(improved_circuit)-1):
                        new_SC[improved_circuit[i],t] = (
                            self.distance_matrix[
                                improved_circuit[i]+1,
                                improved_circuit[i+1]+1]
                            + self.distance_matrix[
                                improved_circuit[i-1]+1,
                                improved_circuit[i]+1]
                            - self.distance_matrix[
                                improved_circuit[i-1]+1,
                                improved_circuit[i+1]+1]
                        )
                    new_SC[improved_circuit[len(improved_circuit)-1],t] =(
                        self.distance_matrix[
                            improved_circuit[len(improved_circuit)-1]+1,
                            0]
                        + self.distance_matrix[
                            improved_circuit[len(improved_circuit)-2]+1,
                            improved_circuit[len(improved_circuit)-1]+1]
                        - self.distance_matrix[
                            improved_circuit[len(improved_circuit)-2]+1,
                            0]
                    )
            for c in range(len(circuits)):
                circuits[c] = [i+1 for i in circuits[c]]
            all_circuits.append(circuits)

        solution["circuits"] = all_circuits
        tmp_sum = 0
        for t in range(self.nb_periods):
            for i in range(len(self.buyers)):
                tmp_sum += (new_SC[i][t] -  SC[i][t]) * solution["z_vars"][i][t]
        to_return = {"objective":obj_value}
        for t in range(self.nb_periods):
            to_return[t]={
                "p":round(solution["p_vars"][t]),
                "y":solution[f"y_vars"][t] ,
                "z":{f"z_{j+1}_{t}":solution["z_vars"][j][t]
                    for j in range(len(self.buyers))},
                "I_vars":{f"I_{j}_{t}":round(solution["I_vars"][j][t])
                    for j in range(len(self.buyers)+1)},
                "q_vars":{f"q_{j}_{t}":round(solution["q_vars"][j][t])
                    for j in range(len(self.buyers))},
                "circuits": solution["circuits"][t]
            }
            to_return[t]["z"].update({f"z_0_{t}": 0})
        self.specify_costs(to_return)
        return to_return

    def specify_costs(self, solution):
        for i in solution:
            if(i != "objective" and i!="gap" and i!="best_bound"):
                tmp = solution[i]
                solution[i]["prod_cost"] = (tmp["p"]
                    *self.producer.production_cost + tmp["y"])
                solution[i]["storage_cost"] = (
                    tmp["I_vars"][f"I_0_{i}"] * self.producer.storage_cost
                    + sum(
                        tmp["I_vars"][f"I_{j}_{i}"]*self.buyers[j].storage_cost
                        for j in range(1,len(self.buyers))
                    )
                )
class Agent:
    def __init__(self, x_coord, y_coord, storage_limit, storage_cost, initial_stock):
        """
            x_coord(int)(xi)
            y_coord(int)(yi)
            storage_limit(int)(Li)
            storage_cost(int)(hi)
        """
        self.x_coord = x_coord
        self.y_coord = y_coord
        self.storage_limit = storage_limit
        self.storage_cost = storage_cost
        self.initial_stock = initial_stock
class Buyer(Agent):
    def __init__(
        self,
        x_coord,
        y_coord,
        storage_limit,
        storage_cost,
        initial_stock,
        demand):
        """
            demand([int])(di)
        """
        super().__init__(x_coord, y_coord, storage_limit, storage_cost, initial_stock)
        self.demand = demand


class Producer(Agent):
    def __init__(
        self,
        x_coord,
        y_coord,
        storage_limit,
        storage_cost,
        setup_cost,
        production_cost,
        production_capacity,
        nb_vehicules,
        vehicule_storage_capacity,
        initial_stock):
        super().__init__(x_coord, y_coord, storage_limit ,storage_cost, initial_stock)

        """
            setup_cost(int)(f)
            production_cost(int)(u)
            production_capacity(int)(C)
            nb_vehicule(int)(k)
            vehicule_storage_capacity(int)(Q)
        """
        self.setup_cost = setup_cost
        self.production_cost = production_cost
        self.production_capacity = production_capacity
        self.nb_vehicules = nb_vehicules
        self.vehicule_storage_capacity = vehicule_storage_capacity
