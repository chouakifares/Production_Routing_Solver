from flask import Flask, request,Response, jsonify
from flask_cors import CORS, cross_origin
from InstanceParser import *
from time import time
import json
import math
app = Flask(__name__)


app.debug = True
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app)
@app.route('/solve', methods=['POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def solve():
    JSON_received = request.get_json()
    solving_method = JSON_received["method"]
    type = JSON_received["type"]
    size = JSON_received["nbNodes"]
    prefix, suffixe = JSON_received["name"].split("_")
    if(size == 14):
        r = InstanceReader("PRP_instances", f"{type}_0{size}_{prefix}_{size+1}_{suffixe}")
    else:
        if(size <100):
            r = InstanceReader("PRP_instances", f"{type}_0{size}_{prefix}_{size}_{suffixe}")
        else:
            r = InstanceReader("PRP_instances", f"{type}_{size}_{prefix}_{size}_{suffixe}")

    instance = r.read_instance()
    if(solving_method == "Branch&Bound"):
        sol = instance.solve_prp_exact()
        create_circuits_from_exact_solution(sol)
    elif(solving_method == "Branch&Bound&Cut"):
        sol = instance.solve_prp_exact(GFSEC = True)
        create_circuits_from_exact_solution(sol)
    else:
        sol = instance.heuristic_solve()

    to_return = {}
    for i in sol:
        if(i!= "objective" and i!="gap" and i!="best_bound"):
            print(sol[i]["circuits"])
            print(sol[i]["q_vars"])
            print(sol[i]["z"])
            print(sol[i]["I_vars"])
            customersToDeliver= []
            nb_buyers = 0
            for j in range(len(sol[i]["z"])-1):
                if sol[i]["z"][f"z_{j+1}_{i}"] == 1 and sol[i]["q_vars"][f"q_{j}_{i}"]>0:
                    nb_buyers+=1
                    index = j+1
                    value = sol[i]["q_vars"][f"q_{j}_{i}"]
                    vehicule = None
                    positionInCycle = None
                    for k in range(len(sol[i]["circuits"])):
                        if j+1 in sol[i]["circuits"][k] :
                            vehicule = k+1
                            positionInCycle = sol[i]["circuits"][k].index(j+1)+1
                    customersToDeliver.append({
                        "index":index,
                        "value":value,
                        "vehicule":vehicule,
                        "positionInCycle":positionInCycle
                    })

            to_return[i] = {
                "prod": sol[i][f"p"],
                "prodCost": sol[i]["prod_cost"],
                "nbTrucks":len(sol[i]["circuits"]),
                "nbBuyers":nb_buyers,
                "customersToDeliver":customersToDeliver,
                "storage":[
                    {
                        "index":j if j!=0 else "Plant",
                        "value":sol[i]["I_vars"][f"I_{j}_{i}"]
                    }
                    for j in range(len(sol[i]["I_vars"]))
                ],
                "storageCost":sol[i]["storage_cost"],
                "circuits":sol[i]["circuits"]
            }
        else:
            to_return[i] = sol[i]

    return Response(json.dumps(to_return),  mimetype='application/json')

def create_circuits_from_exact_solution(sol):

    circuits = {}
    for i in sol:
        if(i!= "objective" and i!="gap" and i!="best_bound"):
            if sol[i]["z"][f"z_0_{i}"] == 0:
                sol[i]["circuits"] =[]
            else:
                sol[i]["circuits"] =[[] for j in range(math.ceil(sol[i]["z"][f"z_0_{i}"]))]
                customers_delivered = [
                    j for j in range(len(sol[i]["z"]))
                    if sol[i]["z"][f"z_{j}_{i}"] == 1 and j
                ]
                pred_succ_list = []
                #looking for the predecessor and successor of each node to construct the cycle
                for customer in customers_delivered:
                    pred, succ = None, None
                    for k in range(len(sol[i]["z"])):
                        if(k != customer):
                            if(sol[i]["x_vars"][f"x_{k}_{customer}_{i}"]):
                                pred = k
                            if(sol[i]["x_vars"][f"x_{customer}_{k}_{i}"]):
                                succ = k
                    pred_succ_list.append((customer,succ, pred))
                #finding the root nodes
                nb_cycles = 0
                for item in pred_succ_list:
                    if item[2] == 0:
                        sol[i]["circuits"][nb_cycles].append(item[0])
                        tmp = tuple(item)
                        while(tmp[1] != 0):
                            sol[i]["circuits"][nb_cycles].append(tmp[1])
                            for tmp_item in pred_succ_list:
                                if(tmp_item[0] == tmp[1]):
                                    tmp = tuple(tmp_item)
                                    break
                        nb_cycles += 1

if __name__ =="__main__":
    app.run(host='localhost')
