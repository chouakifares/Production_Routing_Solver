from InstanceParser import *
from time import time
import os
import random
import pickle
"""m = Model(name='single variable')
x = m.continuous_var(name="x", lb=0)
c1 = m.add_constraint(x >= 2, ctname="const1")
m.set_objective("min", 3*x)
m.print_information()
m.solve()
m.print_solution()

"""
compatible_instances = random.sample([j for j in [i[2] for i in os.walk("PRP_instances/")][0] if j.startswith("A_014")],5)
exact_sol_val, heuristic_sol_val = [], []
bc_times, bb_times = [],[]
for i in compatible_instances:
    r = InstanceReader("PRP_instances",i[:-4])
    instance = r.read_instance()
    t = time()
    exact_sol = instance.solve_prp_exact(False,gap = 0.02, timelimit = 300)
    tmp = time() - t
    bb_times.append((tmp, exact_sol["gap"], exact_sol["objective"]))
    t = time()
    exact_sol = instance.solve_prp_exact(True,gap = 0.02, timelimit = 300)
    tmp = time() - t
    bc_times.append((tmp, exact_sol["gap"], exact_sol["objective"]))

    if(exact_sol["gap"]<0.2):
        exact_sol_val.append(exact_sol["objective"])
    else:
        exact_sol_val.append(exact_sol["best_bound"])
    tmp = exact_sol_val[-1]
    for _ in range(10):
        approx_sol = instance.heuristic_solve()
        tmp = max(tmp, approx_sol["objective"])
        print(tmp)
    heuristic_sol_val.append(tmp)

quality={
    "heuristic":heuristic_sol_val,
    "exact":exact_sol_val
}

speed={
    "B&C":bc_times,
    "B&B":bb_times
}

with open("test_quality.bin","wb") as f:
    pickle.dump(quality, f)

with open("test_speed.bin","wb") as f:
    pickle.dump(speed, f)

"""
r = InstanceReader("PRP_instances", "B_050_instance1")
r.read_instance()
"""
