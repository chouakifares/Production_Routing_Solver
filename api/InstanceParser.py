import re
from Classes import *
class InstanceReader:
    def __init__(self, instance_directory, file_name):
        self.instance_directory = instance_directory
        self.file_name = file_name

    def read_instance(self):
        with open(f'./{self.instance_directory}/{self.file_name}.prp') as f:
            tmp = {}
            buff = [i.strip("\n") for i in f.readlines()]
            for i in buff:
                if(i.startswith("Type")):
                    instance_type = "A" if int(i.strip("Type ")) == 1 else "B"
                elif(i.startswith("n")):
                    nb_buyers = int(i.strip("n "))
                elif(i.startswith("l")):
                    nb_periods = int(i.strip("l "))
                elif(i.startswith("u")):
                    production_cost = int(i.strip("u "))
                elif(i.startswith("f")):
                    setup_cost = int(i.strip("f "))
                elif(i.startswith("C")):
                    production_capacity = int(float(i.strip("C ")))
                elif(i.startswith("k ")):
                    nb_vehicules = int(i.strip("k "))
                elif(i.startswith("Q ")):
                    vehicule_storage_capacity = int(i.strip("Q "))
                elif(i.startswith("mc ")):
                    pass
                elif(i.startswith("0") and ":" in i):
                    (x_coord,
                    y_coord,
                    storage_cost,
                    storage_limit,
                    initial_stock) = re.match(
                        r". ([0-9]+) ([0-9]+) : h ([0-9]+) L (.+) L0 ([0-9]+)",
                        i
                    ).groups()

                    producer = Producer(
                        int(x_coord),
                        int(y_coord),
                        int(float(storage_limit)),
                        int(storage_cost),
                        setup_cost,
                        production_cost,
                        production_capacity,
                        nb_vehicules,
                        vehicule_storage_capacity,
                        int(initial_stock))
                else:
                    if(":" in i):
                        (buyer_index,
                        x_coord,
                        y_coord,
                        storage_cost,
                        storage_limit,
                        initial_stock) = re.match(
                            r"(.+) ([0-9]+) ([0-9]+) : h ([0-9]+) L (.+) L0 ([0-9]+)",
                            i
                        ).groups()
                        tmp[buyer_index] = {
                            "x_coord" : int(x_coord),
                            "y_coord" : int(y_coord),
                            "storage_limit": int(storage_limit),
                            "storage_cost" : int(storage_cost),
                            "initial_stock": int(initial_stock),
                            "demand" : [],

                        }
                    elif(i !="d"):
                        (buyer_index,) = re.match("(.+?) .+?", i).groups()
                        tmp[buyer_index]["demand"] +=[j for j in map(int,[k for k in i[len(buyer_index)+1:len(i)].split(" ") if k != ""])]
            buyers = []
            for i in tmp.keys():
                buyers.append(
                    Buyer(
                        tmp[i]["x_coord"],
                        tmp[i]["y_coord"],
                        tmp[i]["storage_limit"],
                        tmp[i]["storage_cost"],
                        tmp[i]["initial_stock"],
                        tmp[i]["demand"]
                    )
                )

        return Instance(instance_type, nb_periods, producer, buyers)
